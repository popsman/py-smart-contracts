#!/bin/bash

#apt-get update

cd docs/source
sphinx-apidoc -o . ../..
cd ..

sphinx-quickstart --version
make html
